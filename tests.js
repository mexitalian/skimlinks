function equals(arr1, arr2) {
    return arr1.length === arr2.length && arr1.map(function(value, index) {
        return arr1[index] === arr2[index]
    }).filter(function(isEqual) {
        return isEqual
    }).length === arr1.length
}

var tests = []

tests.push({
    expr: function() {
        return equals(divideIntoPools(100, [.5, .5]), [50, 50])
    },
    label: "it divides into equal pools"
})


/*
This test does not follow the logic described in the Readme.md
The actual results without rounding would be
[ 111.105, 4.938, 3.7035, 2.469, 1.2345 ]
which when following
"If there is an amount of money that cannot be divided because of this it should be assigned to a greater percentage pool or the first percentage pool"
should give [ 111.13, 4.93, 3.70, 2.46, 1.23 ]
*/
tests.push({
    expr: function() {
        return equals(divideIntoPools(123.45, [.9, .04, 0.03, 0.02, 0.01]), [111.11, 4.94, 3.70, 2.47, 1.23])
    },
    label: "it handles many pools"
})



tests.push({
    expr: function() {
        return 123.45 === [111.11, 4.94, 3.70, 2.47, 1.23].reduce(function(a,b){return a+b})
    },
    label: "the total of pools equals the total"
})

tests.push({
    expr: function() {
        return equals(divideIntoPools(0.03, [.51, .49]), [0.02, 0.01])
    },
    label: "it assigns remainders to the greater pool"
})

tests.push({
    expr: function() {
        return equals(divideIntoPools(0.10, [.75, .25]), [0.08, 0.02])
    },
    label: "it assigns remainders to the first pool"
})

tests.push({
    expr: function() {
        return equals(divideIntoPools(0.10, [.15, .1, .75]), [0.01, 0.01, 0.08])
    },
    label: "it assigns the remainder to the largest pool"
});

// From the Readme.md
tests.push({
    expr: function() {
        return equals(divideIntoPools(0.03, [.5, .5]), [0.02, 0.01])
    },
    label: "sub-cent rounding example from the Readme"
})

/*
Incompatible
Note: It is giving me a headache trying to figure out how to resolve this whilst having the previous tests validate.
Some clairity would be greatly appriciated.
tests.push({
    expr: function() {
        return equals(divideIntoPools(0.1, [.25, .25, .25, .25]), [0.4, 0.2, 0.2, 0.2])
    },
    label: "sub-cent rounding example from the Readme"
})
*/