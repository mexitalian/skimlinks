function divideIntoPools(money, percentage) {
//TODO: implement
	
	// percentage should add up to 1 (100%)
	if ( percentage.reduce(function(p,c){ return p+c; }) !== 1 ) {
		console.error("The percentages do not add up to 100%");
	}

	var pools = [];
	// var subFract = 0;

	percentage.forEach(function(per) {
/*
		var moneyShare = money*per;
		pools.push( getValidCurrencyUnit(moneyShare) );
		subFract += getNonValidCurrencyUnit(moneyShare);
		*/
		pools.push( getFixedFloat(money*per, 2) );

	});

	// whenever there is an illegal fraction it must be allocated to the first || greater of the pools

	// let's check the total of the pools is equal to the initial money
	var poolTotal = pools.reduce(function(a,b){ return a+b });

	if ( poolTotal !== money ) {

		var poolOver = getFixedFloat( poolTotal - money, 2 ),
			poolOverIsPositive = poolOver > 0,
			largestPoolIndex = getFirstLargestIndex(pools);

		if ( pools.length == 2 ) {

		}


		pools.forEach(function(c,i,a) {
			if ( i > 0 ) {

				if ( poolOverIsPositive ) {
					if ( a[i] <= a[i-1] ) {
						a[i] = getFixedFloat( a[i] - poolOver, 2 );
					} else {
						a[i-1] = getFixedFloat( a[i-1] - poolOver, 2 );
					}
				} else {
					if ( a[i] <= a[i-1] ) {
						a[i-1] = getFixedFloat( a[i-1] - poolOver, 2 );
					} else {
						a[i] = getFixedFloat( a[i] - poolOver, 2 );
					}
				}

			}
		});

	}

	return pools;
}

function getFixedFloat(num,dp) {
	return parseFloat( (num).toFixed(dp) );
}

function getFirstLargestIndex(arr) {

	var maxValue = Math.max.apply(null,arr);

	for ( var i=0; i<arr.length; i++ ) {
		if ( arr[i] === maxValue ) {
			return i;
		}
	}
}


function getValidCurrencyUnit(money) {
	return parseInt(money*100) / 100;
}

function getNonValidCurrencyUnit(money) {
	return money - getValidCurrencyUnit(money);
}
